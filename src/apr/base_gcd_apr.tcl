set TECH "65LP"

source ./proc.tcl
set SCRIPTS_DIR             "."
set RTL_SRC_DIR             "./gcd_w_schain"
set GATE_SRC_DIR            "../syn/no_ro_clk"
set TOP_MODULE              "gcd_w_mux_scan"
set DESIGN_MW_LIB_NAME      "${TOP_MODULE}_lib"
set GATE_NETLIST            "${GATE_SRC_DIR}/netlist/${TOP_MODULE}.syn.sv"
set SDC_FILE                "${GATE_SRC_DIR}/reports/gcd.sdc"

if {[file exist $DESIGN_MW_LIB_NAME]} {
    sh rm -rf $DESIGN_MW_LIB_NAME
}
# set search path, target lib, link path.

if {[string match $TECH "65LP"]} {
   set TSMCPATH /gscratch/ece/pdks/tsmc/N65LP/digital
} elseif {[string match $TECH "65GP"]} {
   set TSMCPATH /gscratch/ece/pdks/tsmc/N65RF/1.0c/digital
} else {	
   puts "The TSMC Path was not defined"
}
if {[string match $TECH "65LP"]} {
	puts "The Cell Lib Path Is:"
	set TARGETCELLLIB $TSMCPATH/Front_End/timing_power_noise/CCS/tcbn65lp_200a
   set target_library "tcbn65lpwc_ccs.db tcbn65lpbc_ccs.db tcbn65lptc_ccs.db"
} elseif {[string match $TECH "65GP"]} {
   puts "The Cell Lib Path Is:"
   set TARGETCELLLIB $TSMCPATH/Front_End/timing_power_noise/CCS/tcbn65gplus_200a
	set target_library "tcbn65lpwc0d9_ccs.db tcbn65lpbc1d1_ccs.db tcbn65lptc1d0_ccs.db"
} else {	
	puts "The Cell Lib Path was not defined"
}

if {[string match $TECH "65LP"]} {
	puts "The Search Path Is:"
   set search_path   [concat  $search_path $TARGETCELLLIB ./db  $synopsys_root/libraries/syn]
   lappend search_path [glob $TSMCPATH/Back_End/milkyway/tcbn65lp_200a/cell_frame/tcbn65lp/LM/*]

} elseif {[string match $TECH "65GP"]} {
   puts "The Search Path Is Path Is:"
   set search_path   [concat  $search_path $TARGETCELLLIB ./db  $synopsys_root/libraries/syn]
   lappend search_path [glob $TSMCPATH/Back_End/milkyway/tcbn65gplus_200a/cell_frame/tcbn65gplus/LM/*]

} else {	
	puts "The Search Path was not defined"
}

if {[string match $TECH "65LP"]} {
	puts "The Link Path Is:"
   set link_path {"*" tcbn65lpwc_ccs.db tcbn65lpbc_ccs.db tcbn65lptc_ccs.db}
} elseif {[string match $TECH "65GP"]} {
   puts "The Link Path Is:"
   set link_path {"*" tcbn65lpwc0d9_ccs.db tcbn65lpbc1d1_ccs.db tcbn65lptc1d0_ccs.db}
} else {	
	puts "The Link Path was not defined"
}

if {[string match $TECH "65LP"]} {
	puts "The Milkyway Path Is:"
   set mw_techfile_path $TSMCPATH/Back_End/milkyway/tcbn65lp_200a/techfiles
	set mw_reference_library $TSMCPATH/Back_End/milkyway/tcbn65lp_200a/frame_only/tcbn65lp
   set mw_tech_file $mw_techfile_path/tsmcn65_9lmT2.tf
	
} elseif {[string match $TECH "65GP"]} {
   puts "The Milkiway Path Is:"
   set mw_techfile_path $TSMCPATH/Back_End/milkyway/tcbn65gplus_200a/techfiles
	set mw_reference_library $TSMCPATH/Back_End/milkyway/tcbn65gplus_200a/frame_only/tcbn65gplus
   set mw_tech_file $mw_techfile_path/tsmcn65_9lmT2.tf
} else {	
	puts "The Link Path was not defined"
}

create_mw_lib $DESIGN_MW_LIB_NAME -technology $mw_tech_file  -mw_reference_library $mw_reference_library  
# open the milkway, by default it is not activated.
open_mw_lib $DESIGN_MW_LIB_NAME

read_verilog -top ${TOP_MODULE} $GATE_NETLIST
link
current_design $TOP_MODULE


#set TSMCPATH /home/lab.apps/vlsiapps/kits/tsmc/N65RF/1.0c_cdb/digital
#set TARGETCELLLIB $TSMCPATH/Front_End/timing_power_noise/CCS/tcbn65gplus_200a
#set search_path   [concat  $search_path $TARGETCELLLIB ./db  $synopsys_root/libraries/syn]
#lappend search_path [glob $TSMCPATH/Back_End/milkyway/tcbn65gplus_200a/cell_frame/tcbn65gplus/LM/*]
#set target_library "tcbn65gpluswc0d72_ccs.db tcbn65gplusbc0d88_ccs.db tcbn65gplustc0d8_ccs.db"
#set symbol_library tcbn65gplustc0d8_ccs.db
#set link_path {"*" tcbn65gpluswc0d72_ccs.db tcbn65gplusbc0d88_ccs.db tcbn65gplustc0d8_ccs.db }

#Create a MW design lib map to techfile, reference library (of stdcell for example)
#Milky way variable settings. Same as topo for the most part.
#set mw_techfile_path /home/lab.apps/vlsiapps/kits/tsmc/N65RF/1.0c_cdb/digital/Back_End/milkyway/tcbn65gplus_200a/techfiles
#set mw_tech_file $mw_techfile_path/tsmcn65_9lmT2.tf
#set mw_reference_library $TSMCPATH/Back_End/milkyway/tcbn65gplus_200a/frame_only/tcbn65gplus
#create_mw_lib -technology $mw_tech_file -mw_reference_library $mw_reference_library gcd_design      
#open_mw_lib gcd_design

#Read in the verilog, uniquify and save the CEL view.
#import_designs fsm.v -format verilog -top fsm
#set up tlu_plus files (for virtual route and post route extraction)
#set_tlu_plus_files \
-max_tluplus $mw_techfile_path/tluplus/cln65g+_1p09m+alrdl_rcbest_top2.tluplus \
-min_tluplus $mw_techfile_path/tluplus/cln65g+_1p09m+alrdl_rcworst_top2.tluplus \
-tech2itf_map $mw_techfile_path/tluplus/star.map_9M

if {[string match $TECH "65LP"]} {
   set_tlu_plus_files \
   -max_tluplus $mw_techfile_path/tluplus/cln65lp_1p09m+alrdl_rcbest_top2.tluplus \
   -min_tluplus $mw_techfile_path/tluplus/cln65lp_1p09m+alrdl_rcworst_top2.tluplus \
   -tech2itf_map $mw_techfile_path/tluplus/star.map_9M
	
} elseif {[string match $TECH "65GP"]} {
   set_tlu_plus_files \
   -max_tluplus $mw_techfile_path/tluplus/cln65g_1p09m+alrdl_rcbest_top2.tluplus \
   -min_tluplus $mw_techfile_path/tluplus/cln65g_1p09m+alrdl_rcworst_top2.tluplus \
   -tech2itf_map $mw_techfile_path/tluplus/star.map_9M
} else {	
	puts "The Link Path was not defined"
}

#perform consistency checks across all libraries (both logic and physical libraries)
set_check_library_options -all
check_library
check_tlu_plus_files

#connect/link all referenced library components to the current design
link

#Read timing and apply constraints
read_sdc ${SDC_FILE}
check_timing

#Check for false or multicycle path settings, any disabled arcs and case_analysis settings
report_timing_requirements
report disable_timing
report_case_analysis

#Define environment
#set_operating_conditions -analysis_type bc_wc  -min BC0D88COM -max WC0D72COM  -max_library tcbn65gpluswc0d72_ccs -min_library tcbn65gplusbc0d88_ccs
#set_min_library tcbn65gpluswc0d72_ccs.db -min_version tcbn65gplusbc0d88_ccs.db
if {[string match $TECH "65LP"]} {

set_operating_conditions -analysis_type bc_wc \
-min BCCOM -max WCCOM  -max_library tcbn65lpwc_ccs \
-min_library tcbn65lpbc_ccs

set_min_library tcbn65lpwc_ccs.db -min_version tcbn65lpbc_ccs.db

} elseif {[string match $TECH "65GP"]} {

set_operating_conditions -analysis_type bc_wc \
-min BC1D1COM -max WC0D9COM  -max_library tcbn65lpwc0d9_ccs \
-min_library tcbn65lpbc1d1_ccs

set_min_library tcbn65gpluswc0d72_ccs.db -min_version tcbn65gplusbc0d88_ccs.db

} else {
	puts "No Operating Conditions Were Defined. Check the TECHNOLOGY Path"
}

#set_driving_cell -lib_cell INVD1 scan_in
#set_driving_cell -lib_cell INVD1 update
#set_driving_cell -lib_cell INVD1 capture
#set_driving_cell -lib_cell INVD1 phi
#set_driving_cell -lib_cell INVD1 phi_bar
#set_driving_cell -lib_cell INVD1 rst
##set_driving_cell -lib_cell INVD1 clk
##set_driving_cell -lib_cell INVD1 cen
#set_driving_cell -lib_cell INVD1 manual_wen
#set_driving_cell -lib_cell INVD1 manual_data_in
#set_driving_cell -lib_cell INVD1 manual_address
#set_driving_cell -lib_cell INVD1 bypass_ctl
#set_driving_cell -lib_cell INVD1 sel_clk
#set_driving_cell -lib_cell INVD1 pad_clk
#set_driving_cell -lib_cell INVD1 ro_en
#set_driving_cell -lib_cell INVD1 ro_I
#set_driving_cell -lib_cell INVD1 ro_final_s

# set_load for outputs
#set_load [load_of tcbn65gpluswc0d72_ccs/INVD1/I] [get_ports Status]
#set_load [load_of tcbn65gpluswc0d72_ccs/INVD1/I] [get_ports Output1]
#set_load [load_of tcbn65gpluswc0d72_ccs/INVD1/I] [get_ports Output2]

if {[string match $TECH "65LP"]} {
set_load [load_of tcbn65lpwc_ccs/INVD1/I] [all_outputs]

} elseif {[string match $TECH "65GP"]} {

set_load [load_of tcbn65lpwc0d9_ccs/INVD1/I] [all_outputs]

} else {
	puts "No Operating Conditions Were Defined. Check the TECHNOLOGY Path"
}

link

#set_case_analysis 1 {scan0/scan_clk_sel}
#set_max_transition 1 [get_designs top] 
## this sets, somehow, the maximum frequency right?
#create_clock -name "pad_clk" -period 10 -waveform {0 1} [get_ports pad_clk]
#set_max_fanout 6 top
#set_clock_uncertainty -setup 0.05 pad_clk
#set_clock_uncertainty -hold 0.01 pad_clk
#set_case_analysis 0 [get_ports {sel_clk}]


#Check clock health and assumptions
report_clock -skew
report_clock

#Apply timing and optimization control. 
#set_dont_use and set_prefer -min<hold_fixing_cells> are likely candidates 
set_app_var timing_enable_multiple_clocks_per_reg true
set_fix_multiple_port_nets -all -buffer_constants
group_path -name INPUTS -from [all_inputs]
group_path -name OUTPUTS -to [all_outputs]

#Check that timing constraints are all good
set_zero_interconnect_delay_mode true
report_constraint -all
report_timing
set_zero_interconnect_delay_mode false

#save current milkyway Design so you can obtain it again with open_mw_cel ....
#save_mw_cel -as gcd_data_setup
save_mw_cel -as gcd_data_setup



#Now you're ready to floorplan the design
# Pinplacement
set PINPLACEMENT_TXT "${SCRIPTS_DIR}/pin_placement.txt"
set PINPLACEMENT_TCL "${SCRIPTS_DIR}/pin_placement.tcl"
exec python ${SCRIPTS_DIR}/gen_pin_placement.py -t ${PINPLACEMENT_TXT} -o ${PINPLACEMENT_TCL}
source ${PINPLACEMENT_TCL}

#******************************************************************************
#**                         02_floorplan                                     **
#******************************************************************************
# ===== Size primitives =====
# SUPER_TILE: definition of a supertile
set CELL_HEIGHT        1.8
set SUPER_TILE         [expr 8*$CELL_HEIGHT]

# ===== Core dimensions =====
# W_SUPER_TILE_NUM: the number of supertiles in core width
# H_SUPER_TILE_NUM: the number of supertiles in core height

set W_SUPER_TILE_NUM   16
set H_SUPER_TILE_NUM   16

set CORE_WIDTH         [expr $W_SUPER_TILE_NUM * $SUPER_TILE]
set CORE_HEIGHT        [expr $H_SUPER_TILE_NUM * $SUPER_TILE]

set CORE_UTIL          "0.8"; # Define initial core utilization (default 0.7)
                            ; # example) 0.6 means 60% utilization.
set FLIP_FIRST_ROW     true ; # true | false by default, flip_first_row is true


#******************************************************************************
#**                         03_powerplan                                     **
#******************************************************************************
# ===== Core power/ground ring (PGR) =====

# POWER_RING_CHANNEL_WIDTH: combined width of the rings, clearances, and spacing
# POWER_RING_CLEARANCE: spacing between adjacent power rings, and between the core and innermost ring edge
# POWER_RING_SPACE: space between the IO pads and outermost ring edge
# POWER_RING_WIDTH: width of a core power ring
# RING_HLAYER: horizontal ring metal
# RING_VLAYER: vertical ring metal
set PGR_CLEARANCE      "2.0"; # spacing between adjacent power rings, and between the core and innermost ring edge
set PGR_SPACE          "1.2"; # space between I/O pads and outermost ring edge
set PGR_WIDTH          "3.0"; # metal width of a core power ring

# one space (pads - vdd) + two metal (vdd, gnd) + two clearance (vdd - gnd, gnd - core)
set PGR_CHANNEL_WIDTH  [expr $PGR_SPACE + 2*$PGR_WIDTH + 2*$PGR_CLEARANCE]

# By default, in tsmc65gp lef,
# {M1, M3, M5, M7, M9} - horizontal {M2, M4, M6, M8, AP} - Vertical
# minimum width of M8, M9 - 0.4
set PGR_HLAYER         "M8"
set PGR_VLAYER         "M9"

#Now you're ready to floorplan the design
create_floorplan -control_type width_and_height \
                 -core_width  [expr $CORE_WIDTH] \
                 -core_height [expr $CORE_HEIGHT] \
                 -left_io2core $PGR_CHANNEL_WIDTH \
                 -right_io2core $PGR_CHANNEL_WIDTH \
                 -top_io2core $PGR_CHANNEL_WIDTH \
                 -bottom_io2core $PGR_CHANNEL_WIDTH \
                 -start_first_row \
                 -flip_first_row
                 #-top_offset 0.5 \
                 #-bottom_offset 0.5



#create_floorplan -control_type aspect_ratio\
                 -core_aspect_ratio 1\
                 -core_utilization 0.8\
                 -left_io2core 3\
                 -right_io2core 3\
                 -top_io2core 3\
                 -bottom_io2core 3

#Power Mapping
derive_pg_connection -power_net VDD -power_pin VDD\
                     -ground_net VSS -ground_pin VSS\
                     -create_ports "top"
derive_pg_connection -power_net VDD -ground_net VSS\
                     -tie
check_mv_design -power_nets



#Plan your power. The outer ring extends out to hit the pin and inner stripe shrinks to cover the core.
 create_rectangular_rings  -nets  {VSS VDD}\
                          -around core\
                          -left_segment_layer M6 \
                          -left_segment_width 1\
                          -right_segment_layer M6\
                          -right_segment_width 1\
                          -bottom_segment_layer M5\
                          -bottom_segment_width 1\
                          -top_segment_layer M5 \
                          -top_segment_width 1\
                          -left_offset 0.5\
                          -right_offset 0.5\
                          -bottom_offset 0.5\
                          -top_offset 0.5


create_power_straps -direction horizontal\
                     -extend_high_ends to_boundary_and_generate_pins  \
                     -extend_low_ends to_boundary_and_generate_pins  \
                     -nets  {VDD VSS}  \
                     -layer M1 \
                     -width 0.33  \
                     -configure rows \
                     -step 3.6 \
                     -pitch_within_group 1.8



#Run placement, fixing timing violations and congestion. 
create_fp_placement 

#save current design
save_mw_cel -as gcd_post_place

#define clock contrains

if {[string match $TECH "65LP"]} {
    set MAX_CLK_CAP tcbn65lpwc_ccs/INVD8/I
} elseif {[string match $TECH "65GP"]} {
    set MAX_CLK_CAP tcbn65gpluswc0d72_ccs/INVD8/I
} else {	
	puts "The Link Path was not defined"
}


set cts_force_user_constraints true
set_max_transition 1 [get_clocks pad_clk]
set_max_transition 1 [get_clocks ro_clk]
set_clock_tree_options  -clock_trees pad_clk\
                        -layer_list {M1 M2 M3 M4 M5 M6 M7}\
                        -target_skew 0.1\
                        -max_capacitance [load_of ${MAX_CLK_CAP}]\
                        -max_transition 0.15\
                        -max_fanout 8\
                        -max_rc_delay 0.3\
                        -use_default_routing_for_sinks 1\
                        -logic_level_balance true

set_clock_tree_options  -clock_trees ro_clk\
                        -layer_list {M1 M2 M3 M4 M5 M6 M7}\
                        -target_skew 0.1\
                        -max_capacitance [load_of ${MAX_CLK_CAP}]\
                        -max_transition 0.15\
                        -max_fanout 8\
                        -max_rc_delay 0.3\
                        -use_default_routing_for_sinks 1\
                        -logic_level_balance true




clock_opt -inter_clock_balance\
          -update_clock_latency\
          -operating_condition max\
          -only_cts

route_zrt_group -all_clock_nets -reuse_existing_global_route true
#CURRENT POINT
#save current design
save_mw_cel -as gcd_post_clk_route

#Fills empty spaces in std cell rows w/ filler cells
insert_stdcell_filler -cell_with_metal {GFILL10 GFILL4 GFILL3 GFILL2 GFILL1}\
                      -connect_to_power "VDD"\
                      -connect_to_ground "VSS"

#Finally, connect all power and ground pins
derive_pg_connection -cells [get_cells *] -power_net VDD -ground_net VSS

#Route
route_opt

# Search & Repair
verify_zrt_route
route_zrt_detail -inc true -initial_drc_from_input true

#Run DRC, LVS
signoff_drc -check_all_layers
list_drc_error_types
set short_errors [get_drc_errors -type {Short}]
verify_lvs

#save the final design
save_mw_cel -as gcd_post_route

#Avoid issues with upper/lower case confusion esp with spice.
define_name_rules STANDARD -case
change_names -rules STANDARD

#Parasitics write out
extract_rc
write_parasitics

#Write out design data
write_def -lef may_need_for_rotated_vias.lef \
          -output "gcd.def"\
          -all_vias

#Write out Verilog of finalized design
write_verilog gcd_apr.v \
              -pg\
              -unconnected_ports

#Write out delay and constraints
write_sdf -context verilog -version 1.0 gcd_apr.sdf
