template : pg_ring(hlay,vlay,width,os) {
   side : horizontal {
      layer : @hlay
      width : @width     #power ring width
      offset : @os    #power ring clearance+width+space
   }
   side : vertical {
      layer : @vlay
      width : @width
      offset : @os
   }
}

