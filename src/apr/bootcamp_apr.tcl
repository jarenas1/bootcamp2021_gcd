set TECH "65LP"

source ./proc.tcl
set SCRIPTS_DIR             "."
set RTL_SRC_DIR             "./gcd_w_schain"
set GATE_SRC_DIR            "../syn/no_ro_clk"
set TOP_MODULE              "gcd_w_mux_scan"
set DESIGN_MW_LIB_NAME      "${TOP_MODULE}_lib"
set GATE_NETLIST            "${GATE_SRC_DIR}/netlist/${TOP_MODULE}.syn.sv"
set SDC_FILE                "${GATE_SRC_DIR}/reports/gcd.sdc"

set PINPLACEMENT_TXT "${SCRIPTS_DIR}/pin_placement.txt"
set PINPLACEMENT_TCL "${SCRIPTS_DIR}/pin_placement.tcl"

if {[file exist $DESIGN_MW_LIB_NAME]} {
    sh rm -rf $DESIGN_MW_LIB_NAME
}


#Setting the Following Variables
#TSMCPATH
#TARGETCELLLIB
#target_library
#search_path
#link_path
#mw_techfile_path
#mw_reference_library
#mw_tech_file
#
if {[string match $TECH "65LP"]} {
    set TSMCPATH /gscratch/ece/pdks/tsmc/N65LP/digital
    set TARGETCELLLIB $TSMCPATH/Front_End/timing_power_noise/CCS/tcbn65lp_200a
    set target_library "tcbn65lpwc_ccs.db tcbn65lpbc_ccs.db tcbn65lptc_ccs.db"
    set search_path   [concat  $search_path $TARGETCELLLIB ./db  $synopsys_root/libraries/syn]
    lappend search_path [glob $TSMCPATH/Back_End/milkyway/tcbn65lp_200a/cell_frame/tcbn65lp/LM/*]
    set link_path {"*" tcbn65lpwc_ccs.db tcbn65lpbc_ccs.db tcbn65lptc_ccs.db}
    set mw_techfile_path $TSMCPATH/Back_End/milkyway/tcbn65lp_200a/techfiles
	set mw_reference_library $TSMCPATH/Back_End/milkyway/tcbn65lp_200a/frame_only/tcbn65lp
    set mw_tech_file $mw_techfile_path/tsmcn65_9lmT2.tf

} elseif {[string match $TECH "65GP"]} {
    set TSMCPATH /gscratch/ece/pdks/tsmc/N65RF/1.0c/digital
    set TARGETCELLLIB $TSMCPATH/Front_End/timing_power_noise/CCS/tcbn65gplus_200a
	set target_library "tcbn65lpwc0d9_ccs.db tcbn65lpbc1d1_ccs.db tcbn65lptc1d0_ccs.db"
    set search_path   [concat  $search_path $TARGETCELLLIB ./db  $synopsys_root/libraries/syn]
    lappend search_path [glob $TSMCPATH/Back_End/milkyway/tcbn65gplus_200a/cell_frame/tcbn65gplus/LM/*]
    set link_path {"*" tcbn65lpwc_ccs.db tcbn65lpbc_ccs.db tcbn65lptc_ccs.db}
    set mw_techfile_path $TSMCPATH/Back_End/milkyway/tcbn65gplus_200a/techfiles
	set mw_reference_library $TSMCPATH/Back_End/milkyway/tcbn65gplus_200a/frame_only/tcbn65gplus
    set mw_tech_file $mw_techfile_path/tsmcn65_9lmT2.tf

} else {	
   puts "The TSMC Path was not defined"
}

create_mw_lib $DESIGN_MW_LIB_NAME -technology $mw_tech_file  -mw_reference_library $mw_reference_library  
# open the milkway, by default it is not activated.
open_mw_lib $DESIGN_MW_LIB_NAME

read_verilog -top ${TOP_MODULE} $GATE_NETLIST
link
current_design $TOP_MODULE

#******************************************************************************
#**                         02_floorplan                                     **
#******************************************************************************
# ===== Size primitives =====
# SUPER_TILE: definition of a supertile
set CELL_HEIGHT        1.8
set SUPER_TILE         [expr 8*$CELL_HEIGHT]

# ===== Core dimensions =====
# W_SUPER_TILE_NUM: the number of supertiles in core width
# H_SUPER_TILE_NUM: the number of supertiles in core height

set W_SUPER_TILE_NUM   18
set H_SUPER_TILE_NUM   18

set CORE_WIDTH         [expr $W_SUPER_TILE_NUM * $SUPER_TILE]
set CORE_HEIGHT        [expr $H_SUPER_TILE_NUM * $SUPER_TILE]

set CORE_UTIL          "0.8"; # Define initial core utilization (default 0.7)
                            ; # example) 0.6 means 60% utilization.
set FLIP_FIRST_ROW     true ; # true | false by default, flip_first_row is true


#******************************************************************************
#**                         03_powerplan                                     **
#******************************************************************************
# ===== Core power/ground ring (PGR) =====

# POWER_RING_CHANNEL_WIDTH: combined width of the rings, clearances, and spacing
# POWER_RING_CLEARANCE: spacing between adjacent power rings, and between the core and innermost ring edge
# POWER_RING_SPACE: space between the IO pads and outermost ring edge
# POWER_RING_WIDTH: width of a core power ring
# RING_HLAYER: horizontal ring metal
# RING_VLAYER: vertical ring metal
set PGR_CLEARANCE      "2.0"; # spacing between adjacent power rings, and between the core and innermost ring edge
set PGR_SPACE          "1.2"; # space between I/O pads and outermost ring edge
set PGR_WIDTH          "3.0"; # metal width of a core power ring

# one space (pads - vdd) + two metal (vdd, gnd) + two clearance (vdd - gnd, gnd - core)
set PGR_CHANNEL_WIDTH  [expr $PGR_SPACE + 2*$PGR_WIDTH + 2*$PGR_CLEARANCE]

# By default, in tsmc65gp lef,
# {M1, M3, M5, M7, M9} - horizontal {M2, M4, M6, M8, AP} - Vertical
# minimum width of M8, M9 - 0.4
set PGR_HLAYER         "M8"
set PGR_VLAYER         "M9"


# POWER NETWORK CONFIG
# ==========================================================================
# - Script expects all of these to be in the run directory

set PG_RING_FILE "pg_ring.tpl"
set PG_MESH_FILE "pg_mesh.tpl"
set RING_VSS_NAME "pg_ring_vss"
set RING_VDD_NAME "pg_ring_vdd"
set LOWER_MESH_NAME "pg_lower_mesh"
set UPPER_MESH_NAME "pg_upper_mesh"



#######################################################
## For Power Definition
#######################################################

set POWER_NET         "VDD"                 ;# This is real power net name.
set GROUND_NET        "VSS"                 ;# This is real ground net name.

set logic1_net        $POWER_NET            ;# Default
set logic0_net        $GROUND_NET           ;# Default

set POWER_PORT        "VDD"                 ;# This is standard cell power pin name.
set GROUND_PORT       "VSS"                 ;# This is standard cell ground pin name.



## Avoiding too many messages
set_message_info -id DPI-025 -limit 1

# Interconnect Technology Format (ITF) defines cross section profile of the process.
# It is an ordered list of conductor and dielectric layer defintion statements. The layers are
# defined from topmost dielectric layer to the bottom most dielectric layer excluding substarte.
if {[string match $TECH "65LP"]} {
   set_tlu_plus_files \
   -max_tluplus $mw_techfile_path/tluplus/cln65lp_1p09m+alrdl_rcbest_top2.tluplus \
   -min_tluplus $mw_techfile_path/tluplus/cln65lp_1p09m+alrdl_rcworst_top2.tluplus \
   -tech2itf_map $mw_techfile_path/tluplus/star.map_9M

    set_operating_conditions -analysis_type bc_wc \
    -min BCCOM -max WCCOM  -max_library tcbn65lpwc_ccs \
    -min_library tcbn65lpbc_ccs
    
    set_min_library tcbn65lpwc_ccs.db -min_version tcbn65lpbc_ccs.db




} elseif {[string match $TECH "65GP"]} {
   set_tlu_plus_files \
   -max_tluplus $mw_techfile_path/tluplus/cln65g_1p09m+alrdl_rcbest_top2.tluplus \
   -min_tluplus $mw_techfile_path/tluplus/cln65g_1p09m+alrdl_rcworst_top2.tluplus \
   -tech2itf_map $mw_techfile_path/tluplus/star.map_9M

    set_operating_conditions -analysis_type bc_wc \
    -min BC1D1COM -max WC0D9COM  -max_library tcbn65lpwc0d9_ccs \
    -min_library tcbn65lpbc1d1_ccs

    set_min_library tcbn65gpluswc0d72_ccs.db -min_version tcbn65gplusbc0d88_ccs.db

} else {	
	puts "The Link Path was not defined"
}

#Interconnect technology/ check layer numbers
echo "***********************************************************************"
echo "                                                                       "
echo "    Check consistency between the Milkyway library and the TLUPlus     "
echo "                                                                       "
echo "***********************************************************************"
#perform consistency checks across all libraries (both logic and physical libraries)
set_check_library_options -all
check_library
check_tlu_plus_files

#Power Mapping
derive_pg_connection -power_net ${POWER_NET} -power_pin ${POWER_PORT}\
                     -ground_net ${GROUND_NET} -ground_pin ${GROUND_PORT}

derive_pg_connection -power_net ${logic1_net} -ground_net ${logic0_net}\
                     -tie
check_mv_design -power_nets

#Read timing and apply constraints
read_sdc ${SDC_FILE}
check_timing

#Check for false or multicycle path settings, any disabled arcs and case_analysis settings
report_timing_requirements
report disable_timing
report_case_analysis

# timing env
# Timing env setting
set timing_enable_multiple_clocks_per_reg true
set case_analysis_with_logic_constants true
set_fix_multiple_port_nets -all -buffer_constants [all_designs]

#Check clock health and assumptions
report_clock -skew
report_clock

#Check that timing constraints are all good
set_zero_interconnect_delay_mode true
report_constraint -all
report_timing
set_zero_interconnect_delay_mode false

#save current milkyway Design so you can obtain it again with open_mw_cel ....
# Save
change_names -rule verilog -hier
set verilogout_no_tri true
save_mw_cel -as ${TOP_MODULE}_setup

# Set Min/Max Routing Layers and routing directions
set MAX_ROUTING_LAYER "M7"
set MIN_ROUTING_LAYER "M1"
set HORIZONTAL_ROUTING_LAYERS "M2 M4 M6 M8 AP"
set VERTICAL_ROUTING_LAYERS "M3 M5 M7 M9"
set_preferred_routing_direction \
    -layers $HORIZONTAL_ROUTING_LAYERS \
    -direction horizontal

set_preferred_routing_direction \
    -layers $VERTICAL_ROUTING_LAYERS \
    -direction vertical

set_route_zrt_common_options -global_max_layer_mode hard

# Min/Max routing layers for the clock
set CLOCK_ROUTING_LAYERS {M1 M2 M3 M4 M5 M6 M7}


exec python ${SCRIPTS_DIR}/gen_pin_placement.py -t ${PINPLACEMENT_TXT} -o ${PINPLACEMENT_TCL}
source ${PINPLACEMENT_TCL}

#Now you're ready to floorplan the design
create_floorplan -control_type width_and_height \
                 -core_width  [expr $CORE_WIDTH] \
                 -core_height [expr $CORE_HEIGHT] \
                 -left_io2core $PGR_CHANNEL_WIDTH \
                 -right_io2core $PGR_CHANNEL_WIDTH \
                 -top_io2core $PGR_CHANNEL_WIDTH \
                 -bottom_io2core $PGR_CHANNEL_WIDTH \
                 -start_first_row \
                 -flip_first_row

# Set Min/Max Routing Layers and routing directions
set MAX_ROUTING_LAYER "M7"
set MIN_ROUTING_LAYER "M1"
set HORIZONTAL_ROUTING_LAYERS "M2 M4 M6 M8 AP"
set VERTICAL_ROUTING_LAYERS "M3 M5 M7 M9"
set_preferred_routing_direction \
    -layers $HORIZONTAL_ROUTING_LAYERS \
    -direction horizontal

set_preferred_routing_direction \
    -layers $VERTICAL_ROUTING_LAYERS \
    -direction vertical

if { $MAX_ROUTING_LAYER != ""} {set_ignored_layers -max_routing_layer $MAX_ROUTING_LAYER}
if { $MIN_ROUTING_LAYER != ""} {set_ignored_layers -min_routing_layer $MIN_ROUTING_LAYER}

set_route_zrt_common_options -global_max_layer_mode hard

# Min/Max routing layers for the clock
set CLOCK_ROUTING_LAYERS {M1 M2 M3 M4 M5 M6 M7}
#Plan your power. The outer ring extends out to hit the pin and inner stripe shrinks to cover the core.
# Clean slate in case we are rerunning
#remove_power_plan_regions -all

# Create power plan regions
# ============================

# Create a power plan region for the core
set core_ppr_name "ppr_core"
create_power_plan_regions $core_ppr_name -core

set hlay $PGR_HLAYER
set vlay $PGR_VLAYER
set width $PGR_WIDTH
set vss_os  $PGR_SPACE ; # offset relative to core edge
set vdd_os [expr $vss_os+$PGR_WIDTH+$PGR_CLEARANCE]

set ring_strategy_name "pg_ring"

# Set the strategies for the rings
set_power_ring_strategy $RING_VSS_NAME \
   -power_plan_regions $core_ppr_name \
   -nets ${GROUND_NET} \
   -template ${SCRIPTS_DIR}/${PG_RING_FILE}:${ring_strategy_name}($hlay,$vlay,$width,$vss_os)

compile_power_plan -ring -strategy $RING_VSS_NAME

set_power_ring_strategy $RING_VDD_NAME \
   -power_plan_regions $core_ppr_name \
   -nets ${POWER_NET} \
   -template ${SCRIPTS_DIR}/${PG_RING_FILE}:${ring_strategy_name}($hlay,$vlay,$width,$vdd_os)

compile_power_plan -ring -strategy $RING_VDD_NAME

set_power_plan_strategy $LOWER_MESH_NAME \
   -power_plan_regions $core_ppr_name \
   -nets {VDD VSS} \
   -template ${SCRIPTS_DIR}/${PG_MESH_FILE}:${LOWER_MESH_NAME}(0,0,0,0,0)

compile_power_plan -strategy $LOWER_MESH_NAME
# Specify the upper mesh separately, since it should hit the core rings
#  - The reason we specify this separately is that the lower mesh should only reach up to
#    the boundary of legal placement, while the upper mesh should keep going and via onto
#    the core rings
set_power_plan_strategy $UPPER_MESH_NAME \
   -power_plan_regions $core_ppr_name \
   -extension [list {nets: {VSS VDD}} [list "stop:" "first_target"]] \
   -nets {VDD VSS} \
   -template ${SCRIPTS_DIR}/${PG_MESH_FILE}:${UPPER_MESH_NAME}(0,0)

# Create core power mesh
compile_power_plan -strategy $UPPER_MESH_NAME


create_power_straps -direction horizontal\
                     -extend_high_ends to_boundary_and_generate_pins  \
                     -extend_low_ends to_boundary_and_generate_pins  \
                     -nets  {VDD VSS}  \
                     -layer M1 \
                     -width 0.33  \
                     -configure rows \
                     -step 3.6 \
                     -pitch_within_group 1.8
create_preroute_vias \
   -nets {VDD VSS} \
   -from_object_strap \
   -to_object_strap \
   -from_layer M2 \
   -to_layer   M1 \
   -advanced_via_rule
# 5% error in voltage is okay.
#synthesize_fp_rail \
	-nets {VDD VSS} -voltage_supply 1.0 \
	-synthesize_power_plan -power_budget 50

#commit_fp_rail

#Run placement, fixing timing violations and congestion.
create_fp_placement -timing_driven -no_hierarchy_gravity

# connect pg net
derive_pg_connection -power_net $POWER_NET -power_pin  $POWER_PORT
derive_pg_connection -ground_net $GROUND_NET -ground_pin $GROUND_PORT
derive_pg_connection -power_net $logic1_net -ground_net $logic0_net -tie

# verify pg connection
verify_pg_nets

# short checking
set_pnet_options -partial "M1 M2 M3 M4 M5 M6 M7 M8"

# legalize
# To resolve cell placement conflicts after doing initial placement, such as violating Standard(STD)
# cells away from the power straps, overlaps, legalize placement.
# command "legalize_fp_placement" is obsolete
legalize_placement -effort high

# Unplace all standard cells except for tap and well_edge cells
remove_placement -object_type standard_cell

set_max_net_length 800 [current_design]

#Identify clock gating and merge any clock gates first
#This performs a blanket merging of the gates. Split can happen later.
#Review this approach at some point in time.
identify_clock_gating
merge_clock_gates
place_opt


#place_opt

# Connect Power & Grounding in extraction and update timing
derive_pg_connection -power_net $POWER_NET -power_pin  $POWER_PORT
derive_pg_connection -ground_net $GROUND_NET -ground_pin $GROUND_PORT
derive_pg_connection -power_net $logic1_net -ground_net $logic0_net -tie

route_zrt_global -congestion_map_only true -effort ultra

# Save
change_names -rule verilog -hier
set verilogout_no_tri true
save_mw_cel -as ${TOP_MODULE}_place


# CTS
#create_clock -name "clk_mem" -period 10 -waveform {0 1} [get_pins {gcd_w_mux0/gcd0/mem/clk}]
#define clock contrains
set cts_force_user_constraints true

#set_clock_tree_options  -clock_trees clk_mem\
#                        -layer_list ${CLOCK_ROUTING_LAYERS}\
#                        -target_skew 0.1\
#                        -max_capacitance [load_of tcbn65lpwc_ccs/INVD8/I]\
#                        -max_transition 0.15\
#                        -max_fanout 8\
#                        -max_rc_delay 0.3\
#                        -use_default_routing_for_sinks 1\
#                        -logic_level_balance true
#compile_clock_tree -clock_trees clk_mem



#set_clock_tree_options  -clock_trees pad_clk\
#                        -layer_list ${CLOCK_ROUTING_LAYERS}\
#                        -target_skew 0.1\
#                        -max_capacitance [load_of tcbn65lpwc_ccs/INVD8/I]\
#                        -max_transition 0.15\
#                        -max_fanout 8\
#                        -max_rc_delay 0.3\
#                        -use_default_routing_for_sinks 1\
#                        -logic_level_balance true
#
#set_clock_tree_options  -clock_trees ro_clk\
#                        -layer_list ${CLOCK_ROUTING_LAYERS}\
#                        -target_skew 0.1\
#                        -max_capacitance [load_of tcbn65lpwc_ccs/INVD8/I]\
#                        -max_transition 0.15\
#                        -max_fanout 8\
#                        -max_rc_delay 0.3\
#                        -use_default_routing_for_sinks 1\
#                        -logic_level_balance true


#set_clock_tree_exceptions\
#   -clocks [get_clocks]\
#   -float_pins [get_ports {clk_o}]\
#   -float_pin_max_delay_rise 0.040\
#   -float_pin_min_delay_rise 0.040



#set_clock_tree_exceptions\
    -stop_pins


clock_opt -inter_clock_balance\
          -update_clock_latency\
          -operating_condition max\
          -only_cts -no_clock_route


derive_pg_connection -power_net $POWER_NET -power_pin  $POWER_PORT
derive_pg_connection -ground_net $GROUND_NET -ground_pin $GROUND_PORT
derive_pg_connection -power_net $logic1_net -ground_net $logic0_net -tie

clock_opt -no_clock_route -only_psyn -area_recovery -power

derive_pg_connection -power_net $POWER_NET -power_pin  $POWER_PORT
derive_pg_connection -ground_net $GROUND_NET -ground_pin $GROUND_PORT
derive_pg_connection -power_net $logic1_net -ground_net $logic0_net -tie

route_zrt_group -all_clock_nets -reuse_existing_global_route true

redirect -file ./reports/cts/clock_tree_settings.rpt   {report_clock_tree -settings}
redirect -file ./reports/cts/clock_tree_exceptions.rpt {report_clock_tree -exceptions}
redirect -file ./reports/cts/skew.rpt                  {report_clock_tree}
redirect -file ./reports/cts/skew.summary.rpt          {report_clock_tree -summary}
redirect -file ./reports/cts/clock_structure.rpt       {report_clock_tree -structure}

# Save
change_names -rule verilog -hier
set verilogout_no_tri true
save_mw_cel -as ${TOP_MODULE}_cts


#Route
route_opt -initial_route_only -effort high

# Search & Repair
verify_zrt_route
route_zrt_detail -inc true -initial_drc_from_input true

insert_zrt_redundant_vias

verify_zrt_route
route_zrt_detail -inc true -initial_drc_from_input true

route_opt \
-skip_initial_route \
-effort high

verify_zrt_route
route_zrt_detail -inc true -initial_drc_from_input true

derive_pg_connection -power_net $POWER_NET -power_pin  $POWER_PORT
derive_pg_connection -ground_net $GROUND_NET -ground_pin $GROUND_PORT
derive_pg_connection -power_net $logic1_net -ground_net $logic0_net -tie
verify_pg_nets

#Run DRC, LVS
signoff_drc -check_all_layers
list_drc_error_types
set short_errors [get_drc_errors -type {Short}]

# Check LVS
verify_lvs -max_error 500

#Fills empty spaces in std cell rows w/ filler cells
insert_stdcell_filler -cell_without_metal {FILL10 FILL4 FILL3 FILL2 FILL1}\
                      -ignore_soft_placement_blockage \
                      -between_std_cells_only \
                      -connect_to_power $POWER_NET\
                      -connect_to_ground $GROUND_NET

# Save
change_names -rule verilog -hier
set verilogout_no_tri true
save_mw_cel -as ${TOP_MODULE}_route

#Parasitics write out
extract_rc
update_timing
write_parasitics



#Avoid issues with upper/lower case confusion esp with spice.
# Case sensitive (avoid issues with spice)
define_name_rules STANDARD -case
change_names -rules STANDARD


# SPEF
write_parasitics -format SPEF \
   -output ./results/$TOP_MODULE.apr.spef \
   -no_name_mapping

# LEF+DEF
write_def -lef ./results/$TOP_MODULE.lef \
          -output ./results/$TOP_MODULE.def \
          -all_vias

#Write out Verilog of finalized design
write_verilog ./results/${TOP_MODULE}_no_pg.apr.v \
              -unconnected_ports \
              -no_core_filler_cells

write_verilog ./results/$TOP_MODULE.apr.sv \
              -pg\
              -unconnected_ports \
              -no_core_filler_cells

#Write out delay and constraints

write_sdf -context verilog -version 1.2 ./results/$TOP_MODULE.sdf
write_sdc  -version 1.7 -nosplit ./results/$TOP_MODULE.sdc

# report
# Area
report_area -physical -hier -nosplit > "./reports/area.rpt"

# Power and backannotation
report_power -verbose -hier -nosplit > "./reports/power.hier.rpt"
report_power -verbose -nosplit > "./reports/power.rpt"
# Floorplanning and placement
report_fp_placement > "./reports/placement.rpt"

# Clocking
report_clock_tree -nosplit > "./reports/clocktree.rpt"

# QoR
report_qor -nosplit > "./reports/qor.rpt"

report_timing -path end  -delay max -max_paths 200 -nosplit > "./reports/paths.max.rpt"
report_timing -path full -delay max -max_paths 50  -nosplit > "./reports/full_paths.max.rpt"
report_timing -path end  -delay min -max_paths 200 -nosplit > "./reports/paths.min.rpt"
report_timing -path full -delay min -max_paths 50  -nosplit > "./reports/full_paths.min.rpt"

redirect -file ./reports/check_legality { check_legality -verbose }
redirect -file ./reports/constraints.rpt { report_constraint \
	-all_violators -verbose -nosplit -significant_digits 3 }
redirect -file ./reports/max_timing.rpt {
	report_timing -significant_digits 3 \
	-delay max -transition_time  -capacitance \
	-max_paths 20 -nets -input_pins \
	-physical -attributes -nosplit -derate -crosstalk_delta -derate -path full_clock_expanded
}
redirect -file ./reports/min_timing.rpt {
	report_timing -significant_digits 3 \
	-delay min -transition_time  -capacitance \
	-max_paths 20 -nets -input_pins \
	-physical -attributes -nosplit -crosstalk_delta -derate -path full_clock_expanded
}




