import random
import numpy as np
import os
import fire
import math
class GCD(object):
    def mem_gen(self,mem_size,N,in_file,in_file_hex):
        max_num = -1 + 2**N
        unit_size = math.floor(mem_size/3)
        IN_AB = np.random.randint(max_num-1,size=2*unit_size)+1
        IN_zero = np.zeros(mem_size-2*unit_size)
        IN_MX = np.concatenate([IN_AB,IN_zero]).astype(int)
        np.savetxt(in_file,np.reshape(IN_AB,(unit_size,2)),fmt="%d",delimiter=",")
        hex_digits = np.ceil(np.log2(np.amax(IN_MX))/4)
        if (hex_digits < 10):
            hex_fmt = "%0" + str(hex_digits) + "X"
        else:
            hex_fmt = "%" + str(hex_digits) + "X"
            
        #np.savetxt(in_file_hex,IN_MX,fmt='%03X',delimiter="\n")
        np.savetxt(in_file_hex,IN_MX,fmt=hex_fmt,delimiter="\n")
    def check(self,in_file,sv_file,out_file,iter_file):
        iter_data = np.loadtxt(iter_file,dtype=int)
        #print(iter_data)
        initial_point = int(iter_data[0]/2)
        iterations = iter_data[1]
        in_data = np.loadtxt(in_file,dtype=int, delimiter=",")
        #print (in_data)
        A = in_data[initial_point:initial_point+iterations,0]
        B = in_data[initial_point:initial_point+iterations,1]
        #print(A)
        A = A.reshape(1,A.size)
        B = B.reshape(1,B.size)
        gcd_total = np.transpose(np.gcd(A,B))
        gcd_ideal = gcd_total
        gcd_ideal = gcd_ideal.reshape(1,gcd_ideal.size)
        #print(initial_point)
        #print(iterations+initial_point)
        print(gcd_ideal.size)
        gcd_sv = np.loadtxt(sv_file,dtype=int).reshape(1,gcd_ideal.size)
        gcd_error = gcd_ideal - gcd_sv
        gcd_error = gcd_error.reshape(1,gcd_error.size)
        #print(gcd_sv.size)
        #print(A.size)
        #print(gcd_error.size)
        gcd_check = np.array_equal(gcd_ideal,gcd_sv)
        gcd_matrix = np.stack((A,B,gcd_ideal,gcd_sv,gcd_error),axis=-1).reshape(A.size,5)
        print("This is the GCD Result Matrix")
        print("A \t B \t py \t sv \t error")
        print(gcd_matrix)
        np.savetxt(out_file,gcd_matrix,fmt="%d",delimiter=',',header='IN_A,IN_B,PYTHON,SVERILOG,ERROR')

        if(gcd_check==True):
            print("GREAT! ALL THE NUMBERS MATCHED")
        else:
            print("OH NO! THERE ARE SOME ERRORS IN THE IMPLEMENTATION")
    
    def scan_check(self,scan_file):
        f = open(scan_file,"r")
        lines = f.readlines()
        i=0
        for x in lines:
            if(x != "\n" and i>0):
                print(x)
            else:
                pass
            i=i+1

if __name__ == '__main__':
    fire.Fire(GCD)
