set MYLIBNAME "gcd_design"
set RTLPATH /gscratch/psylab/usr/jarenas/bootcamp2021_gcd/src/rtl/gcd/gcd_no_ro/w_schain
set TECH "65LP"
#set TECH "65GP"


if {[string match $TECH "65LP"]} {
	puts "The TSMC Path Is:"
	set TSMCPATH /gscratch/ece/pdks/tsmc/N65LP/digital
	puts "The Cell Lib Path Is:"
	set TARGETCELLLIB $TSMCPATH/Front_End/timing_power_noise/CCS/tcbn65lp_200a
	
	set target_library "tcbn65lpwc_ccs.db tcbn65lpbc_ccs.db tcbn65lptc_ccs.db"
	set symbol_library tcbn65lptc_ccs.db
	set link_path {"*" tcbn65lpwc_ccs.db tcbn65lpbc_ccs.db tcbn65lptc_ccs.db }

	set mw_techfile_path $TSMCPATH/Back_End/milkyway/tcbn65lp_200a/techfiles
	set mw_reference_library $TSMCPATH/Back_End/milkyway/tcbn65lp_200a/frame_only/tcbn65lp
	
	set_tlu_plus_files \
	-max_tluplus $mw_techfile_path/tluplus/cln65lp_1p09m+alrdl_rcbest_top2.tluplus \
	-min_tluplus $mw_techfile_path/tluplus/cln65lp_1p09m+alrdl_rcworst_top2.tluplus \
	-tech2itf_map $mw_techfile_path/tluplus/star.map_9M

} elseif {[string match $TECH "65GP"]} {
	puts "The TSMC Path Is:"
	set TSMCPATH /gscratch/ece/pdks/tsmc/N65RF/1.0c/digital
	puts "The Cell Lib Path Is:"
	set TARGETCELLLIB $TSMCPATH/Front_End/timing_power_noise/CCS/tcbn65gplus_200a

	set target_library "tcbn65lpwc0d9_ccs.db tcbn65lpbc1d1_ccs.db tcbn65lptc1d0_ccs.db"
	set symbol_library tcbn65lptc1d0_ccs.db
	set link_path {"*" tcbn65lpwc0d9_ccs.db tcbn65lpbc1d1_ccs.db tcbn65lptc1d0_ccs.db }

	set mw_techfile_path $TSMCPATH/Back_End/milkyway/tcbn65gplus_200a/techfiles
	set mw_reference_library $TSMCPATH/Back_End/milkyway/tcbn65gplus_200a/frame_only/tcbn65gplus

	set_tlu_plus_files \
	-max_tluplus $mw_techfile_path/tluplus/cln65g+_1p09m+alrdl_rcbest_top2.tluplus \
	-min_tluplus $mw_techfile_path/tluplus/cln65g+_1p09m+alrdl_rcworst_top2.tluplus \
	-tech2itf_map $mw_techfile_path/tluplus/star.map_9M

} else {
	puts "The TSMC Path was not defined"
	puts "The Cell Lib Path was not defined"
}



set search_path   [concat  $search_path $TARGETCELLLIB ./db  $synopsys_root/libraries/syn]

lappend search_path [glob $TSMCPATH/Back_End/milkyway/tcbn65lp_200a/cell_frame/tcbn65lp/LM/*]

# Technical file: It has the technology description *.tf file
set mw_tech_file $mw_techfile_path/tsmcn65_9lmT2.tf

if {[file exist $MYLIBNAME]} {
    sh rm -rf $MYLIBNAME
}

create_mw_lib -technology $mw_tech_file -mw_reference_library $mw_reference_library $MYLIBNAME
# open the milkway, by default it is not activated.
open_mw_lib $MYLIBNAME

read_file $RTLPATH -autoread -top gcd_w_mux_scan -format sverilog



if {[string match $TECH "65LP"]} {

set_operating_conditions -analysis_type bc_wc \
-min BCCOM -max WCCOM  -max_library tcbn65lpwc_ccs \
-min_library tcbn65lpbc_ccs

set_min_library tcbn65lpwc_ccs.db -min_version tcbn65lpbc_ccs.db

} elseif {[string match $TECH "65GP"]} {

set_operating_conditions -analysis_type bc_wc \
-min BC1D1COM -max WC0D9COM  -max_library tcbn65lpwc0d9_ccs \
-min_library tcbn65lpbc1d1_ccs

set_min_library tcbn65gpluswc0d72_ccs.db -min_version tcbn65gplusbc0d88_ccs.db

} else {
	puts "No Operating Conditions Were Defined. Check the TECHNOLOGY Path"
}


set_driving_cell -lib_cell INVD1 rstn
set_driving_cell -lib_cell INVD1 pad_clk
set_driving_cell -lib_cell INVD1 ro_clk
set_driving_cell -lib_cell INVD1 sel_clk
set_driving_cell -lib_cell INVD1 gcd_start
set_driving_cell -lib_cell INVD1 bypass_ctl
set_driving_cell -lib_cell INVD1 manual_wen
set_driving_cell -lib_cell INVD1 manual_address
set_driving_cell -lib_cell INVD1 manual_data_in
set_driving_cell -lib_cell INVD1 scan_in
set_driving_cell -lib_cell INVD1 update
set_driving_cell -lib_cell INVD1 capture
set_driving_cell -lib_cell INVD1 phi
set_driving_cell -lib_cell INVD1 phi_bar




set_disable_timing [get_cells scan0/*]
#set_false_path -from scan0/*
#set_false_path -from phi*
#set_false_path -from capture
#set_false_path -from update
#set_false_path -from gcd0/mem/*

#set_dont_touch [get_cells {gcd0/mem/*}]
#set_dont_touch [get_cells {scan0/*}]
set_ideal_network [get_nets {rstn}] -no_propagate

if {[string match $TECH "65LP"]} {
set_load [load_of tcbn65lpwc_ccs/INVD1/I] [all_outputs]
} elseif {[string match $TECH "65GP"]} {
set_load [load_of tcbn65lpwc0d9_ccs/INVD1/I] [all_outputs]
} else {
	puts "No Operating Conditions Were Defined. Check the TECHNOLOGY Path"
}


link

#set_case_analysis 1 {scan0/scan_clk_sel}

set_max_transition 1 [get_designs gcd_w_mux_scan] 
set_max_fanout 6 gcd_w_mux_scan
set_case_analysis 0 [get_ports {sel_clk}]

#create_clock -name "pad_clk" -period 10 -waveform {0 1} [get_ports gcd_w_mux0/gcd0/clk]
#create_clock -name "ro_clk" -period 10 -waveform {0 1} -add [get_ports gcd_w_mux0/gcd0/clk]


create_clock -name "pad_clk" -period 100 -waveform {0 1} [get_ports pad_clk]
create_clock -name "ro_clk" -period 100 -waveform {0 1} [get_ports ro_clk]
#create_generated_clock -name "clk" -source [get_ports pad_clk] -divide_by 1 -add [get_ports clk_o] -combinational
#create_clock -name "ro_clk" -period 10 [get_ports{gcd_w_mux0/clk}]
#create_clock -name "pad_clk" -period 10 -add [get_ports{gcd_w_mux0/clk}]
#set_clock_groups -logically_exclusive -group ro_clk -group pad_clk
#set_ideal_network [get_ports {pad_clk}]
#set_ideal_network [get_ports {ro_clk}]

#create_generated_clock -name "clk" -source [get_ports pad_clk] -add [get_ports clk_sync0/clk_out] -divide_by 1

#create_clock -name "clk1" -period 5 -waveform {0 1} [get_ports ro_clk]
set_clock_uncertainty -setup 0.05 [get_clocks]
set_clock_uncertainty -hold 0.01 [get_clocks]
set_clock_transition 0.05 [get_clocks]
set_input_delay 0.1 -clock pad_clk [remove_from_collection [all_inputs] [get_ports [list gcd_w_mux0/gcd0/clk]]]
set_input_delay 0.1 -clock ro_clk [remove_from_collection [all_inputs] [get_ports [list gcd_w_mux0/gcd0/clk]]]
#set_input_delay 0.1 -clock clk [remove_from_collection [all_inputs] [get_ports [list clk]]]
#set_fix_hold {clk}

set_clock_gating_style \
-sequential_cell latch \
-control_point before \
-minimum_bitwidth 4 \
-max_fanout 128 \
-positive_edge_logic {integrated} \
-setup 0.01 -hold 0.01


#Compile ultra will take care of ungrouping and flattening for improved performance. 
set_critical_range 0.04 $current_design
compile_ultra -no_autoungroup -incremental -gate_clock
check_design


#write out design files
file mkdir reports
report_power > reports/gcd.power
report_constraint -verbose > reports/gcd.constraint
report_constraint -all_violators > reports/gcd.violation
report_timing -path full -delay max -max_paths 5   -nworst 2 > "reports/timing.max.fullpath.rpt"
report_timing -path full -delay min -max_paths 5   -nworst 2 > "reports/timing.min.fullpath.rpt"  
report_area -hierarchy > "reports/area.rpt"

write_sdc  reports/gcd.sdc
file mkdir db
write -h gcd_w_mux_scan -output ./db/gcd.db
write_sdf -context verilog -version 1.0 reports/gcd.sdf
file mkdir netlist
write -h -f verilog gcd_w_mux_scan -output netlist/gcd_w_mux_scan.syn.sv -pg
file mkdir ddc
write_file -format ddc -hierarchy -output ddc/DIG_TOP.ddc
