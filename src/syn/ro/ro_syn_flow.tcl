set MYLIBNAME "ro_design"
set RTLPATH "/gscratch/psylab/usr/jarenas/bootcamp2021_gcd/src/rtl/ro"
set TECH "65LP"
#set TECH "65GP"


if {[string match $TECH "65LP"]} {
	puts "The TSMC Path Is:"
	set TSMCPATH /gscratch/ece/pdks/tsmc/N65LP/digital
	puts "The Cell Lib Path Is:"
	set TARGETCELLLIB $TSMCPATH/Front_End/timing_power_noise/CCS/tcbn65lp_200a
	
	set target_library "tcbn65lpwc_ccs.db tcbn65lpbc_ccs.db tcbn65lptc_ccs.db"
	set symbol_library tcbn65lptc_ccs.db
	set link_path {"*" tcbn65lpwc_ccs.db tcbn65lpbc_ccs.db tcbn65lptc_ccs.db }

	set mw_techfile_path $TSMCPATH/Back_End/milkyway/tcbn65lp_200a/techfiles
	set mw_reference_library $TSMCPATH/Back_End/milkyway/tcbn65lp_200a/frame_only/tcbn65lp
	
	set_tlu_plus_files \
	-max_tluplus $mw_techfile_path/tluplus/cln65lp_1p09m+alrdl_rcbest_top2.tluplus \
	-min_tluplus $mw_techfile_path/tluplus/cln65lp_1p09m+alrdl_rcworst_top2.tluplus \
	-tech2itf_map $mw_techfile_path/tluplus/star.map_9M

} elseif {[string match $TECH "65GP"]} {
	puts "The TSMC Path Is:"
	set TSMCPATH /gscratch/ece/pdks/tsmc/N65RF/1.0c/digital
	puts "The Cell Lib Path Is:"
	set TARGETCELLLIB $TSMCPATH/Front_End/timing_power_noise/CCS/tcbn65gplus_200a

	set target_library "tcbn65lpwc0d9_ccs.db tcbn65lpbc1d1_ccs.db tcbn65lptc1d0_ccs.db"
	set symbol_library tcbn65lptc1d0_ccs.db
	set link_path {"*" tcbn65lpwc0d9_ccs.db tcbn65lpbc1d1_ccs.db tcbn65lptc1d0_ccs.db }

	set mw_techfile_path $TSMCPATH/Back_End/milkyway/tcbn65gplus_200a/techfiles
	set mw_reference_library $TSMCPATH/Back_End/milkyway/tcbn65gplus_200a/frame_only/tcbn65gplus

	set_tlu_plus_files \
	-max_tluplus $mw_techfile_path/tluplus/cln65g+_1p09m+alrdl_rcbest_top2.tluplus \
	-min_tluplus $mw_techfile_path/tluplus/cln65g+_1p09m+alrdl_rcworst_top2.tluplus \
	-tech2itf_map $mw_techfile_path/tluplus/star.map_9M

} else {
	puts "The TSMC Path was not defined"
	puts "The Cell Lib Path was not defined"
}



set search_path   [concat  $search_path $TARGETCELLLIB ./db  $synopsys_root/libraries/syn]

lappend search_path [glob $TSMCPATH/Back_End/milkyway/tcbn65lp_200a/cell_frame/tcbn65lp/LM/*]

# Technical file: It has the technology description *.tf file
set mw_tech_file $mw_techfile_path/tsmcn65_9lmT2.tf


create_mw_lib -technology $mw_tech_file -mw_reference_library $mw_reference_library $MYLIBNAME
# open the milkway, by default it is not activated.
open_mw_lib $MYLIBNAME

read_file $RTLPATH -autoread -top ro -format sverilog



if {[string match $TECH "65LP"]} {

set_operating_conditions -analysis_type bc_wc \
-min BCCOM -max WCCOM  -max_library tcbn65lpwc_ccs \
-min_library tcbn65lpbc_ccs

set_min_library tcbn65lpwc_ccs.db -min_version tcbn65lpbc_ccs.db

} elseif {[string match $TECH "65GP"]} {

set_operating_conditions -analysis_type bc_wc \
-min BC1D1COM -max WC0D9COM  -max_library tcbn65lpwc0d9_ccs \
-min_library tcbn65lpbc1d1_ccs

set_min_library tcbn65gpluswc0d72_ccs.db -min_version tcbn65gplusbc0d88_ccs.db

} else {
	puts "No Operating Conditions Were Defined. Check the TECHNOLOGY Path"
}


set_driving_cell -lib_cell INVD1 en
set_driving_cell -lib_cell INVD1 I
set_driving_cell -lib_cell INVD1 final_s


#set_ideal_network [get_nets {rst}] -no_propagate

if {[string match $TECH "65LP"]} {
set_load [load_of tcbn65lpwc_ccs/INVD1/I] [all_outputs]
#set_load [load_of tcbn65lpwc_ccs/INVD1/I] [get_ports scan_out]
#set_load [load_of tcbn65lpwc_ccs/INVD1/I] [get_ports gcd_out]
#set_load [load_of tcbn65lpwc_ccs/INVD1/I] [get_ports finish_gcd]

} elseif {[string match $TECH "65GP"]} {

set_load [load_of tcbn65lpwc_ccs/INVD1/I] [all_outputs]
} else {
	puts "No Operating Conditions Were Defined. Check the TECHNOLOGY Path"
}


link

#set_case_analysis 1 {scan0/scan_clk_sel}
#set_max_transition 1 [get_designs ro] 
## this sets, somehow, the maximum frequency right?
#set_max_fanout 6 top
#set_case_analysis 0 [get_ports {cell_control}]
#create_clock -name "pad_clk" -period 10 -waveform {0 1} [get_ports pad_clk]
#$create_clock -name "ro_clk" -period 100 -waveform {0 1} [get_ports ro_clk]
#set_clock_groups -logically_exclusive -group ro_clk -group pad_clk
#set_ideal_network [get_ports {pad_clk}]
#set_ideal_network [get_ports {ro_clk}]

#create_generated_clock -name "clk" -source [get_ports pad_clk] -add [get_ports clk_sync0/clk_out] -divide_by 1

#create_clock -name "clk1" -period 5 -waveform {0 1} [get_ports ro_clk]
#set_clock_uncertainty -setup 0.05 [get_clocks]
#set_clock_uncertainty -hold 0.01 [get_clocks]
#set_clock_transition 0.05 [get_clocks]
#set_input_delay 0.1 -clock pad_clk [remove_from_collection [all_inputs] [get_ports [list pad_clk]]]
#set_input_delay 0.1 -clock ro_clk [remove_from_collection [all_inputs] [get_ports [list ro_clk]]]
#set_input_delay 0.1 -clock clk [remove_from_collection [all_inputs] [get_ports [list clk]]]
#set_fix_hold {clk}


#set_disable_timming [get_ports {mux_in}]
#set_fix_multiple_port_nets -outputs -buffer_constants
#set_fix_multiple_port_nets -no_rewire
set compile_advanced_fix_multiple_port_nets true
set_fix_multiple_port_nets -all
#set_dont_touch [get_ports {mux_in}]
#Compile ultra will take care of ungrouping and flattening for improved performance. 
set_critical_range 0.04 $current_design

#set compile_fix_multiple_port_nets TRUE
compile_ultra -no_autoungroup
check_design


#write out design files
file mkdir reports
report_power > reports/ro.power
report_constraint -verbose > reports/ro.constraint
report_constraint -all_violators > reports/ro.violation
report_timing -path full -delay max -max_paths 5   -nworst 2 > "reports/timing.max.fullpath.rpt"
report_timing -path full -delay min -max_paths 5   -nworst 2 > "reports/timing.min.fullpath.rpt"  
report_area -hierarchy > "reports/area.rpt"

write_sdc  reports/ro.sdc
file mkdir db
write -h ro -output ./db/ro.db
write_sdf -context verilog -version 1.0 reports/ro.sdf
file mkdir netlist
write -h -f verilog ro -output netlist/ro_syn.v -pg
file mkdir ddc
write_file -format ddc -hierarchy -output ddc/DIG_TOP.ddc
