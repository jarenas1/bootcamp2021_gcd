module tb_ro;

localparam N=8;
localparam STAGES = 7;


logic en;
logic [$clog2(STAGES)-1:0] I;
logic [$clog2(N)-1:0] final_s;
logic ro_clk;
logic [STAGES+2:0] mux_out;
logic [STAGES+2:0] nand_out;
logic [N-1:0] count;


ro #(.N(N),.STAGES(STAGES)) ro0 (
    .en(en),
    .I(I),
    .final_s(final_s),
    .ro_clk(ro_clk),
    .mux_out(mux_out),
    .nand_out(nand_out),
    .count(count)
);


initial begin
$vcdpluson;
$vcdplusmemon;
en <= '0;
I <= '0;
final_s <= '0;
#100;
en <= 1'b1;
//#100;
//en <= '0;
//I <= 5;
//#100;
//en <= 1'b1;
//#100;
#1000;
final_s <= 4;
#1100;
$finish;

end

endmodule
