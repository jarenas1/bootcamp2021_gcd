`timescale 1ns/10ps
module tb ;

//Parameters//
localparam width = 8;
localparam ad_width = 8;
localparam ro_stages = 7;

//Top Module Logic //
logic rstn;
logic pad_clk;
logic ro_clk;
logic sel_clk;
logic ro_en;
logic gcd_start;
logic bypass_ctl;
logic manual_wen;
logic [ad_width-1:0] manual_address;
logic [ad_width-1:0] manual_data_in;
//Scan Chain Logic
logic scan_in;
logic update;
logic phi;
logic phi_bar;
logic capture;
logic scan_out;
//Top Module Outputs
logic valid;
logic finish_gcd;
logic [width-1:0] gcd_out;


//Scan chain I/O defs
logic [2:0] scan_ro_I, sc_scan_ro_I;
logic [2:0] scan_ro_final_s, sc_scan_ro_final_s;
logic [ad_width-1:0] scan_gcd_iterations;
logic [ad_width-1:0] scan_gcd_initial_point;
logic [ad_width-1:0] scan_gcd_offset;
logic [ad_width-1:0] scan_gcd_final_address;
logic scan_gcd_cen;
//Other Definitions
int i,j; //tb counters
int f,gcd_inputs; //txt files
logic end_count;
logic  [width-1:0] tb_mem [2**ad_width-1:0];


// Clock Definition //

always
begin
    pad_clk <= 1; #5;
    pad_clk <= 0; #5;
end
always
begin
    ro_clk <= 1; #10;
    ro_clk <= 0; #20;
end


logic rst;
assign rstn = ~rst; //work with active high on the tb

gcd_w_mux_scan #(.width(width),.ad_width(ad_width)) dut
(
    .rstn(rstn),
    .pad_clk(pad_clk),
    .ro_clk(ro_clk),
    .sel_clk(sel_clk),
    .gcd_start(gcd_start),
    .bypass_ctl(bypass_ctl),
    .manual_wen(manual_wen),
    .manual_address(manual_address),
    .manual_data_in(manual_data_in),
    .scan_in(scan_in),
    .update(update),
    .capture(capture),
    .phi(phi),
    .phi_bar(phi_bar),
    .valid(valid),
    .finish_gcd(finish_gcd),
    .gcd_out(gcd_out),
    .scan_ro_I(sc_scan_ro_I),
    .scan_ro_final_s(sc_scan_ro_final_s),
    .scan_out(scan_out)
    );



// Include the Scan Chain Tasks //
`include "./scan_module_gcd_tasks.v"

task print_iter;
     gcd_inputs = $fopen("./gcd_inputs.verify.iter","w");
     $display(scan_gcd_iterations); 
     $fwrite(gcd_inputs,"%d\n",scan_gcd_initial_point);
     $fwrite(gcd_inputs,"%d\n",scan_gcd_iterations);
     $fclose(gcd_inputs);
endtask

task load_memory;
     j = 0;
     end_count = '0;
     manual_wen = '0;
     manual_data_in = '0;
     manual_address = '0; 
     $readmemh("./gcd_inputs.verify.hex",tb_mem);
     #10;
     bypass_ctl = 1'b1;
     #5120;
endtask


always @(negedge pad_clk) begin
    if (bypass_ctl & ~end_count & j<=2**ad_width-1) begin
            manual_data_in = tb_mem[j];
            manual_wen = 1'b1;
            manual_address = j;
            j = j + 1;
    end
    else if (bypass_ctl & ~end_count & j>2**ad_width-1 && j<=2*(2**ad_width-1)) begin
            manual_data_in = '0;
            //manual_data_in = '0;
            manual_wen = '0;
            manual_address = j-(2**ad_width);
            j = j + 1;
    end

    else if (j>=2*(2**ad_width-1)) begin
            end_count = 1'b1;
            bypass_ctl = '0;
    end
end


always @(posedge valid)
begin
    i = i + 1;
    //if (i>=20) begin
    //    #2;
    //    sel_clk = 1; //change the clock on the fly
    //end

    if(valid) begin  
        #3;
        $display(gcd_out);
        $fwrite(f,"%d\n",gcd_out); 

        if(finish_gcd) begin
            $display("Max number of iterations Reached");
            $fclose(f);
            $display("Getting and Printing the Scan Chain Outputs Report");
            scan_outputs();
            report_results();
            $finish;
        end
        #3;
    end
end


initial 
begin
     $dumpfile("simulation_output.vcd");
     $dumpvars(0,tb);
    //Set the tb counters to 0
     j = 0;
     i = 0;
    //load the gcd with the default schain values //
     set_default_test_variables();
    //print the number of iterations into a txt file (for python) //
     print_iter();
    //set an initial point for scan chain signals // 
     rst <=1;
     gcd_start <= '0;
     phi <=0;
     phi_bar <=0;
     update <=0;
     capture <=0;
     scan_inputs(); //scan the inputs
     
    //bypass mode to load the memory (note that the gcd is still in rst state)
     bypass_ctl <= '0;
     sel_clk <= '0;
     load_memory();

    //open the dump file
     f = $fopen("./gcd_outputs.verify","w");
     rst <=1; 
     #20;
     rst<=0;
     #30;
     gcd_start <= 1'b1; //Start the GCD computation
     #100000000;
     $finish;

end


endmodule
