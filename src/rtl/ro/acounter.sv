module acounter #(parameter N=11) (
    input logic en,
    input logic c_in,
    output logic [N-1:0] count
);

//initial count = '0;

always_ff @(posedge c_in or negedge en) begin
    if(~en) begin
        count <= '0;
    end
    else begin
        count <= count + 1'b1;
    end
end

//always @(*) begin
//    if(~en) begin
//        count <= '0;
//    end
//    else if (en & ~c_in) begin
//        count <= count;
//    end
//    else begin
//        count <= count + 1'b1;
//    end
//end

endmodule
