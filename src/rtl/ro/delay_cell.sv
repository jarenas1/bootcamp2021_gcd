module delay_cell (
    input logic nand_in,
    input logic select,
    input logic select_nex,
    input logic mux_in,
    output logic nand_out,
    output logic mux_out
);

assign nand_out = ~(nand_in&select);
assign mux_out = (~select_nex & select) ? ~nand_out : ~mux_in;

endmodule
