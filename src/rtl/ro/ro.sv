module ro #(parameter N=11, parameter STAGES=7)(
    input logic en,
    input logic [$clog2(STAGES)-1:0] I,
    input logic [$clog2(N)-1:0] final_s,
    output logic ro_clk,
    output logic [STAGES+2:0] mux_out,
    output logic [STAGES+2:0] nand_out,
    output logic [N-1:0] count

);
logic cin,nand_out_msb;
logic [STAGES-1:0] maskn;
logic [STAGES+2:0] dec_out,select,select_nex,mux_in; //including the 3 extra lsbs for default stages
assign maskn = '0;
assign dec_out = {~((~maskn) << I),3'b111};
assign ro_clk = count[final_s];

`ifdef DC
    nand (cin,en,mux_out[0]); // delay for presynthesis
`else
    nand #10 (cin,en,mux_out[0]); // delay for presynthesis
`endif

//assync counter
acounter #(.N(N)) ac0 (
   .en(en),
   .c_in(cin),
   .count(count)
);

assign nand_out_msb = nand_out[STAGES+2];
assign select = dec_out;
assign select_nex = {1'b0,dec_out[STAGES+2:1]};
assign mux_in = {nand_out_msb,mux_out[STAGES+2:1]};
//Delay_cell Chain
delay_cell delay_cell_i[STAGES+2:0](
    .nand_in({nand_out[STAGES+1:0],cin}),
    .select(select), //
    .select_nex(select_nex), //
    .mux_in(mux_in),//
    .nand_out(nand_out),//
    .mux_out(mux_out)//
);



endmodule
