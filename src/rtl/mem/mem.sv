module mem #(parameter width = 8,parameter ad_width=8)(
    
    //integer mem_word = $clog2(3*iterations);
    
    
    input logic clk,
    input logic WEN,
    input logic CEN,
    input logic [ad_width-1:0] address,
    input logic [width-1:0] data_in,
    output logic [width-1:0] data_out

);

logic  [width-1:0] mem [2**ad_width-1:0];

always @(posedge clk)
begin
    
    if (~WEN) begin
        mem[address] <= data_in;
        data_out <= mem[address];
    end
    else begin
        mem[address] <= mem[address];
        data_out <= mem[address];
    end


//    data_out <= mem[address];

end

endmodule

