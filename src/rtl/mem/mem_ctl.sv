`timescale 1ns/10ps

module mem_ctl #(parameter width = 8, parameter ad_width = 8)(
    //localparam mem_word = $clog2(3*iterations);
    input logic clk,
    input logic rstn,
    input logic start,
    input logic valid,
    input logic [ad_width-1:0] offset,
    input logic [width-1:0] data_out,
    output logic fire,
    output logic stop,
    input logic [ad_width-1:0] initial_point,
    input logic [ad_width-1:0] iterations,
    output logic [ad_width-1:0] address,
    output logic [ad_width-1:0] final_address,
    output logic [width-1:0] in_a,
    output logic [width-1:0] in_b

);

logic  en_address, en_a, en_b, en_offset, en_count;
logic load_counter, d_counter; 
logic [1:0] count; 
logic [1:0] o_count;
logic [ad_width-1:0] int_address, d_address,d_offset,q_offset,iter_reg,iter_count;
logic [width-1:0] d_a, d_b;
logic control;
logic count_m;
logic stop_en;
assign final_address = int_address;
typedef enum logic [2:0] {init,upa,loada_upb,loadb,upb,trig,hold} State;

assign control = start & ~stop;
State current, next;

always_ff @(posedge clk) //state register
begin
    if(~rstn) current <= init;
    else current <= next;
end

always_comb // state_logic
begin
    case(current)
        init:
            if(start) next = upa;
            else next = init;
        upa:
            next = loada_upb;
        loada_upb:
            next = loadb;
        loadb:
            next = upb;
        upb:
            next = trig;
        trig:
            next = hold;
        hold:
            if(valid) next = init;
            else next = hold;
        default: next = init;
    endcase
end

always_comb // output logic
begin
    case(current)
        init: begin
            fire = '0;
            en_a = '0;
            en_b = '0;
            en_address = '0;
            en_offset = '0;
        end
        upa: begin
            fire = '0;
            en_a = '0;
            en_b = '0;
            en_address = 1'b1; 
            en_offset = '0;
        end
        loada_upb: begin
            fire = '0;
            en_a = 1'b1;
            en_b = '0;
            en_address = '0;
            en_offset = '0;
        end 
        loadb: begin
            fire = '0;
            en_a = '0;
            en_b = 1'b1;
            en_address = 1'b1;
            en_offset = '0;
        end
        upb: begin
            fire = '0;
            en_a = '0;
            en_b = '0;
            en_address = '0;
            en_offset = '0;
        end 
        trig: begin
            fire = 1'b1;
            en_a = '0;
            en_b = '0;
            en_address = '0; 
            en_offset =1'b1;
        end

        hold: begin
            fire = '0;
            en_a = '0;
            en_b = '0;
            en_address = '0; 
            en_offset =1'b1;            
        end

        default: begin
            fire = '0;
            en_a = '0;
            en_b = '0;
            en_address = '0; 
            en_offset =1'b1;            
        end

    endcase
end


always_ff @(posedge clk or negedge rstn) // Load Regs and Counter
begin
    if(~rstn) begin
        in_a <= '0;
        in_b <= '0;
        int_address <= initial_point;
        q_offset <= '0;
        iter_reg <= 8'd0;
        stop <= '0;
    end
    else begin
        in_a <= d_a;
        in_b <= d_b;
        int_address <= d_address;
        q_offset <= d_offset;
        iter_reg <= iter_count; 
        //if(rstn & fire) begin
        //iter_reg <= iter_reg+1'b1;
        //end
        //else begin
        //iter_reg <= iter_reg;
        //end
        if(stop_en) begin
            stop <= stop_en;
        end
        else begin
            stop <= stop;
        end
    end

end

always_comb
    begin
        if(rstn&fire) begin
            iter_count = iter_reg+1'b1;
        end
        else begin
            iter_count = iter_reg;
        end
    end


always_comb
begin

    if(en_a) d_a = data_out;
    else d_a = in_a;
    
    if(en_b) d_b = data_out;
    else d_b = in_b; 

    if(en_address) d_address = int_address + 1'b1;
    else d_address = int_address;


    if(en_offset) d_offset = offset-2'd2;
    else d_offset = '0;

    address = int_address + q_offset;

    

    /// Iteration counter that triggers with fire
    //if(fire) iter_count = iter_reg+1'b1;
    //else iter_count = iter_reg;

    if(iter_reg == iterations) stop_en = 1'b1;
    else stop_en = '0;

    

end    


endmodule
