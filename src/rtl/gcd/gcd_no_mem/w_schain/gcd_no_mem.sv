module gcd_no_mem #(parameter width = 8, parameter ad_width=8)(
    input logic clk,
    input logic rstn,
    input logic start,
    input logic cen,
    input logic [ad_width-1:0] initial_point,
    input logic [ad_width-1:0] iterations,
    input logic [ad_width-1:0] offset,
    input logic [ad_width-1:0] manual_address,
    input logic [width-1:0] manual_data_in,
    input logic bypass_ctl,
    input logic manual_wen,
    output logic [ad_width-1:0] final_address,
    output logic valid,
    output logic finish_gcd, 
    output logic [width-1:0] gcd_out,
    //Memory  IO
    input logic [width-1:0] data_out,
    output logic [ad_width-1:0] address,
    output logic WEN,
    output logic [width-1:0] data_in

);

logic fire;
logic [width-1:0] in_a, in_b;
logic [ad_width-1:0] ctl_address;




always_comb
begin
    address = bypass_ctl ? manual_address : ctl_address;
    WEN = bypass_ctl ? ~manual_wen : ~valid;
    data_in = bypass_ctl ? manual_data_in : gcd_out;
end

gcd#(.width(width)) gcd
(
    .clk(clk),
    .rstn(rstn),
    .fire(fire),
    .IN_A(in_a),
    .IN_B(in_b),
    .valid(valid),
    .GCD_OUT(gcd_out)
);


//mem#(.width(width),.ad_width(ad_width)) mem
//(
//    .clk(clk),
//    .WEN(WEN),
//    .CEN(cen),
//    .address(address),
//    .data_in(data_in),
//    .data_out(data_out)
//);

mem_ctl #(.width(width),.ad_width(ad_width)) mem_ctl
(
    .clk(clk),
    .rstn(rstn),
    .valid(valid),
    .start(start),
    .offset(offset),
    .data_out(data_out),
    .fire(fire),
    .initial_point(initial_point),
    .iterations(iterations),
    .address(ctl_address),
    .final_address(final_address),
    .stop(finish_gcd),
    .in_a(in_a),
    .in_b(in_b)
);


endmodule
