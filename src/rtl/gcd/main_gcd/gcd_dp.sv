typedef struct packed { 
            logic en_a;
            logic en_b;
            //logic  en_out;
            logic  sel_a;
            logic  sel_b;
} dp_in;

module gcd_dp #(parameter width = 10)
(
    input logic clk,
    input logic rstn,
    input logic [width-1:0] IN_A,
    input logic [width-1:0] IN_B,
    input dp_in dp_inputs,
    input logic valid_en,
    output logic valid,    
    output logic [width-1:0] GCD_OUT,
    output logic comp
);

logic [width-1:0] Da,Db,Qa,Qb,Dout,Qout;
logic [width-1:0] subtraction_a,subtraction_b;

assign GCD_OUT = Qout;

always_ff @(posedge clk) //Create the registers
begin
    if(~rstn) begin
        Qa <='0;
        Qb <='0;
        Qout <='0;
        valid <= '0;
    end
    else begin
        Qa <= Da;
        Qb <= Db;
        Qout <= Dout;
        valid <= valid_en;
    end
end

always_comb
begin
    if (Qa > Qb) begin
        subtraction_a = Qa-Qb;
        subtraction_b = Qb;
        comp = '0;
    end
    else if (Qa < Qb) begin
        subtraction_a = Qa;
        subtraction_b = Qb-Qa;
        comp = '0;
    end
    else begin
        subtraction_a = Qa;
        subtraction_b = Qb;
        comp = 1'b1;
    end

    if (dp_inputs.en_a && dp_inputs.sel_a) Da = subtraction_a; 
    else if (dp_inputs.en_a && !dp_inputs.sel_a) Da = IN_A;
    else Da = Qa;
    

    if (dp_inputs.en_b && dp_inputs.sel_b) Db = subtraction_b; 
    else if (dp_inputs.en_b && !dp_inputs.sel_b) Db = IN_B;
    else Db = Qb;

    if (comp && rstn) Dout = Qa;
    else Dout = Qout;
end



endmodule
