`timescale 1ns/10ps
module gcd #(parameter width = 10)(


    input logic clk,
    input logic rstn,
    input logic fire,
    input logic [width-1:0] IN_A,
    input logic [width-1:0] IN_B,
    output logic valid, 
    output logic [width-1:0] GCD_OUT
);


typedef struct packed{ 
            logic en_a;
            logic en_b;
            logic  sel_a;
            logic  sel_b;
}fsm_dp_con;

logic comp,valid_en;
fsm_dp_con fsm_dp;

gcd_dp #(.width(width)) gcd_dp
(
      .clk(clk),
      .rstn(rstn),
      .IN_A(IN_A),
      .IN_B(IN_B),
      .dp_inputs(fsm_dp),
      .valid(valid),
      .valid_en(valid_en),
      .GCD_OUT(GCD_OUT),
      .comp(comp)
);


gcd_fsm gcd_fsm(
    .clk(clk),
    .rstn(rstn),
    .fire(fire),
    .comp(comp),
    .fsm_out(fsm_dp),
    .valid(valid_en)
    );

endmodule
