`timescale 1ns/10ps

typedef struct packed { 
            logic en_a;
            logic en_b;
            //logic  en_out;
            logic  sel_a;
            logic  sel_b;
}fsm_o;

module gcd_fsm(
    input logic clk,
    input logic rstn,
    input logic fire,
    input logic comp,
    output  fsm_o fsm_out,
    output logic valid
);



logic valid_f;

assign valid = (valid_f);
typedef enum logic [1:0] {hold,compare,update} State;

State current, next;

always_ff @(posedge clk) //state register
begin
    if(~rstn) current <= hold;
    else current <= next;
end

always_comb // state_logic
begin
    case(current)
        hold:
            if(fire) next = compare;
            else next = hold;
        
        compare:
            if(comp) next = update;
            else next = compare;

        update:
            next = hold;
        
        default: 
            next = hold;
        
    endcase
end

always_comb
begin
    case(current)
        hold: begin
           fsm_out.en_a = 1'b1;
           fsm_out.en_b = 1'b1;
           valid_f = '0;
           fsm_out.sel_a = '0;
           fsm_out.sel_b = '0;
        end
        compare: begin
           fsm_out.en_a = 1'b1;
           fsm_out.en_b = 1'b1;
           valid_f = '0;
           fsm_out.sel_a = 1'b1;
           fsm_out.sel_b = 1'b1;
        end
        update: begin
          fsm_out.en_a = '0;
          fsm_out.en_b = '0;
          valid_f = 1'b1;
          fsm_out.sel_a = '0;
          fsm_out.sel_b = '0;
        end
        default: begin
           fsm_out.en_a = 1'b1;
           fsm_out.en_b = 1'b1;
           valid_f = '0;
           fsm_out.sel_a = '0;
           fsm_out.sel_b = '0;
        end

    endcase
end


endmodule
