module top #(parameter width = 8, parameter ad_width = 8, parameter ro_stages=7) (
    input logic rstn,
    input logic pad_clk,
    input logic sel_clk,
    // ring oscillator enable//
    input logic ro_en,
    // gcd start signal //
    input logic gcd_start,
    // Memory bypass control //
    input logic bypass_ctl,
    input logic manual_wen,
    input logic [ad_width-1:0] manual_address,
    input logic [width-1:0] manual_data_in,
    // End of Memory bypass control //
    // Scan Chain Inputs //
    input logic scan_in,
    input logic update,
    input logic capture,
    input logic phi,
    input logic phi_bar,
    // GCD outputs //
    output logic valid,
    output logic finish_gcd,
    output logic [width-1:0] gcd_out,
    // Scan Chain Outputs //
    output logic scan_out

);

// Scan chain logic for ro
logic [2:0] scan_ro_I, scan_ro_final_s;
// Scan chain logic for gcd
logic [7:0] scan_gcd_offset, scan_gcd_initial_point;
logic [7:0] scan_gcd_iterations;
logic [7:0] scan_gcd_final_address;
logic scan_gcd_cen;

gcd_w_ro #(.width(width),.ad_width(ad_width),.ro_stages(ro_stages)) gcd_w_ro0 (

    .rstn(rstn),
    .pad_clk(pad_clk),
    .sel_clk(sel_clk),
    .ro_en(ro_en),
    .ro_I(scan_ro_I),
    .ro_final_s(scan_ro_final_s),
    .cen(scan_gcd_cen),
    .offset(scan_gcd_offset),
    .bypass_ctl(bypass_ctl),
    .manual_wen(manual_wen),
    .manual_address(manual_address),
    .manual_data_in(manual_data_in),
    .initial_point(scan_gcd_initial_point),
    .iterations(scan_gcd_iterations),
    .start(gcd_start),
    .final_address(scan_gcd_final_address),
    .valid(valid),
    .finish_gcd(finish_gcd),
    .gcd_out(gcd_out)

);

scan_module_gcd scan0 (
    .scan_in(scan_in),
    .update(update),
    .capture(capture),
    .phi(phi),
    .phi_bar(phi_bar),
    .scan_out(scan_out),
    .scan_gcd_cen(scan_gcd_cen),
    .scan_gcd_offset(scan_gcd_offset),
    .scan_gcd_initial_point(scan_gcd_initial_point),
    .scan_gcd_iterations(scan_gcd_iterations),
    .scan_ro_I(scan_ro_I),
    .scan_ro_final_s(scan_ro_final_s),
    .scan_gcd_final_address(scan_gcd_final_address)
);

endmodule
