module gcd_w_mux #(parameter width = 8, parameter ad_width = 8, parameter ro_stages=7)(
      input logic rstn,
      input logic pad_clk,
      input logic ro_clk, 
      input logic sel_clk, // 0 for pad_clk and 1 for ro_clk
      //ro_clk control//
      //input logic ro_en,
      //input logic [$clog2(ro_stages)-1:0] ro_I,
      //input logic [2:0] ro_final_s, // keep a counter of 8 bits
      //end of ro_clk control //
      //memory bypass control
      input logic cen,
      input logic [ad_width-1:0] offset,
      input logic bypass_ctl,
      input logic manual_wen,
      input logic [ad_width-1:0] manual_address,
      input logic [width-1:0] manual_data_in,
      //end of memory bypass control
      //
      //memory control
      input logic [ad_width-1:0] initial_point,
      input logic [ad_width-1:0] iterations,
      input logic start,
      output logic [ad_width-1:0] final_address,
      output logic clk_o,
      //
      //end of memory control
      //GCD outputs
      output logic valid,
      output logic finish_gcd, //Leave it for debugging
      output logic [width-1:0] gcd_out //Leave it for debugging
      //end of GCD outputs
      );


//clk logic//
logic clk;

//Define the Clock Mux//
assign clk = sel_clk ? ro_clk : pad_clk;
//assign clk = (ro_clk & sel_clk) | (pad_clk & ~sel_clk);
assign clk_o = sel_clk ? ro_clk : pad_clk;
gcd_w_mem#(.width(width),.ad_width(ad_width)) gcd0
(
      .clk(clk),
      .rstn(rstn),
      .start(start),
      .initial_point(initial_point),
      .iterations(iterations),
      .final_address(final_address),
      .valid(valid),
      .finish_gcd(finish_gcd),
      .cen(cen),
      .offset(offset),
      .bypass_ctl(bypass_ctl),
      .manual_data_in(manual_data_in),
      .manual_address(manual_address),
      .manual_wen(manual_wen),
      .gcd_out(gcd_out)
);

endmodule
